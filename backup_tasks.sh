#!/bin/bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
mkdir -p ${SCRIPT_DIR}/backups

cp ${SCRIPT_DIR}/tasks.yml backups/$(date +%Y%m%d_%H%M%S)_tasks.yml
