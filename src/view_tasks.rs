use schedule::{print_header, print_task, print_tasks_json, Task};

pub fn view_tasks(tasks: &Vec<Task>, name: Option<&str>, status: Option<&str>, max_duration: Option<&str>, min_duration: Option<&str>, class: Option<&str>, max_occurrences: Option<&str>, min_occurrences: Option<&str>, tags: Vec<String>, json: bool) {
    if !json {
        print_header();
    }
    for task in tasks {
        match name {
            Some(n) if !&task.name.contains(n) => continue,
            _ => (),
        };
        match status {
            Some(s) if s != &task.status => continue,
            _ => (),
        };
        match max_duration {
            Some(d) if d.parse::<i64>().expect("max-duration must be an integer") < task.duration => continue,
            _ => (),
        };
        match min_duration {
            Some(d) if d.parse::<i64>().expect("min-duration must be an integer") > task.duration => continue,
            _ => (),
        };
        match class {
            Some(c) if c != &task.class => continue,
            _ => (),
        };
        match max_occurrences {
            Some(o) if o.parse::<usize>().expect("max-occurrences must be an integer") < task.occurrences.len() => continue,
            _ => (),
        };
        match min_occurrences {
            Some(o) if o.parse::<usize>().expect("min-occurrences must be an integer") > task.occurrences.len() => continue,
            _ => (),
        };
        let mut contains_tag = false;
        if tags.len() > 0 {
            for tag in &tags {
                if task.tags.contains(tag) {
                    contains_tag = true;
                    break;
                }
            }
            if !contains_tag {
                continue;
            }
        }
        if !json {
            print_task(task);
        }
    }
    if json {
        print_tasks_json(tasks);
    }
}
