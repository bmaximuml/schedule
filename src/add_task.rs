use schedule::{Task, TaskYaml};

pub fn add_task(
    mut tasks: TaskYaml, name: String, status: String, duration: i64, class: String, tags: Vec<String>, verbose: bool
) -> Result<TaskYaml, String> {
    let names: Vec<&String> = tasks.tasks.iter().map(|t| &t.name).collect();
    if names.contains(&&name) {
        Err(format!("Task {} already exists", name))
    }
    else {
        if verbose {
            println!("Adding task with name: {}", name);
            println!("    Task duration: {} minutes", duration.to_string());
            println!("    Task status: {}", status);
            println!("    Task class: {}", class);
        }

        tasks.tasks.push(
            Task {
                name,
                duration,
                status,
                class: class,
                occurrences: Vec::new(),
                tags: tags,
            },
        );
        Ok(tasks)
    }
}