use schedule::{print_header, print_task, print_task_json, Task};
use rand::seq::SliceRandom;


pub fn choose_task(tasks: &Vec<Task>, name: Option<&str>, status: Option<&str>, max_duration: Option<&str>, min_duration: Option<&str>, class: Option<&str>, max_occurrences: Option<&str>, min_occurrences: Option<&str>, tags: Vec<String>, json: bool) {
    let mut valid_choices = Vec::new();
    for task in tasks {
        let mut valid = true;
        match name {
            Some(n) if !&task.name.contains(n) => valid = false,
            _ => (),
        };
        match status {
            Some(s) if s != &task.status => valid = false,
            _ => (),
        };
        match max_duration {
            Some(d) if d.parse::<i64>().expect("max-duration must be an integer") < task.duration => valid = false,
            _ => (),
        };
        match min_duration {
            Some(d) if d.parse::<i64>().expect("min-duration must be an integer") > task.duration => valid = false,
            _ => (),
        };
        match class {
            Some(c) if c != &task.class => valid = false,
            _ => (),
        };
        match max_occurrences {
            Some(o) if o.parse::<usize>().expect("max-occurrences must be an integer") < task.occurrences.len() => valid = false,
            _ => (),
        };
        match min_occurrences {
            Some(o) if o.parse::<usize>().expect("min-occurrences must be an integer") > task.occurrences.len() => valid = false,
            _ => (),
        };
        let mut contains_tag = false;
        if tags.len() > 0 {
            for tag in &tags {
                if task.tags.contains(tag) {
                    contains_tag = true;
                    break;
                }
            }
            if !contains_tag {
                valid = false;
            }
        }
        if valid {
            valid_choices.push(task);
        }
    }

    let choice = valid_choices.choose(&mut rand::thread_rng());

    match &choice {
        Some(c) => {
            if !json {
                print_task_json(c);
            } else {
                print_header();
                print_task(c);
            }
        },
        _ => println!("No valid task for filters")
    }
}
