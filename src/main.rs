extern crate chrono;
extern crate clap;

mod add_task;
mod choose_task;
mod view_tasks;
mod update_task;

use chrono::{Datelike, Timelike, Local, DateTime};
use clap::{App, load_yaml};

use schedule::{dump_yaml_serde, load_yaml_serde, report_error, TaskOccurrence, TaskYaml};

use crate::add_task::add_task;
use crate::choose_task::choose_task;
use crate::update_task::update_task;
use crate::view_tasks::view_tasks;

fn main() {
    // The YAML file is found relative to the current file, similar to how modules are found
    let cli = load_yaml!("cli.yml");
    let matches = App::from_yaml(cli).get_matches();

    // Gets a value for tasks file if supplied by user, or defaults to "tasks.yml"
    let tasks_file: &str = matches.value_of("tasks").unwrap_or("tasks.yml");
    let mut tasks: TaskYaml = load_yaml_serde(tasks_file);

    let verbose = matches.is_present("verbose");
    let json = matches.is_present("json");

    if let Some(matches) = matches.subcommand_matches("viewTasks") {
        let name = matches.value_of("name");
        let status = matches.value_of("status");
        let max_duration = matches.value_of("max-duration");
        let min_duration = matches.value_of("min-duration");
        let class = matches.value_of("class");
        let max_occurrences = matches.value_of("max-occurrences");
        let min_occurrences = matches.value_of("min-occurrences");
        let tags = matches.values_of("tag").unwrap_or_default().collect::<Vec<&str>>();
        // Convert tags from &str to String
        let tags = tags.iter().map(|tag| String::from(*tag)).collect();
        view_tasks(&tasks.tasks, name, status, max_duration, min_duration, class, max_occurrences, min_occurrences, tags, json);
    } else if let Some(matches) = matches.subcommand_matches("addTask") {
        // Add new task
        let name = String::from(matches.value_of("name").unwrap());
        let status = String::from(matches.value_of("status").unwrap());
        let class = String::from(matches.value_of("class").unwrap());
        let duration = matches.value_of("duration").unwrap().parse::<i64>().unwrap();
        let tags = matches.values_of("tag").unwrap_or_default().collect::<Vec<&str>>();
        // Convert tags from &str to String
        let tags = tags.iter().map(|tag| String::from(*tag)).collect();

        tasks = match add_task(tasks, name, status, duration, class, tags, verbose) {
            Ok(t) => t,
            Err(error) => panic!("{}", report_error(error)),
        };
    } else if let Some(matches) = matches.subcommand_matches("addTasksTags") {
        let names = matches.values_of("name").unwrap_or_default().collect::<Vec<&str>>();
        // let name = String::from(matches.value_of("name").unwrap());
        let tags = matches.values_of("tag").unwrap_or_default().collect::<Vec<&str>>();
        let mut found = false;
        
        for task in &mut tasks.tasks {
            if names.contains(&task.name.as_str()) {
                found = true;
                for tag in &tags {
                    if !task.tags.contains(&tag.to_string()) {
                        task.tags.push(tag.to_string());
                    }
                }
                break;
            }
        }
        
        if verbose {
            if found {
                println!("Added tag(s) {:?} to task(s) {:?}", tags, names);
            }
            else {
                println!("Could not find task(s): {:?}", names);
            }
        }
    } else if let Some(matches) = matches.subcommand_matches("removeTaskTags") {
        let name = String::from(matches.value_of("name").unwrap());
        let tags = matches.values_of("tag").unwrap_or_default().collect::<Vec<&str>>();
        let mut found = false;
        
        let mut removed_tags = Vec::new();
        for task in &mut tasks.tasks {
            if task.name == name {
                found = true;
                for tag in &tags {
                    if task.tags.contains(&tag.to_string()) {
                        let index = &task.tags.iter().position(|t| t == tag);
                        removed_tags.push(match index {
                            Some(i) => task.tags.remove(*i),
                            _ => continue,
                        });
                    }
                }
                break;
            }
        }
        
        if verbose {
            if found {
                println!("Removed tag(s) {:?} from task {}", removed_tags, name);
            }
            else {
                println!("Could not find task: {}", name);
            }
        }
    } else if let Some(matches) = matches.subcommand_matches("deleteTags") {
        let tags = matches.values_of("tag").unwrap_or_default().collect::<Vec<&str>>();
        for task in &mut tasks.tasks {
            for tag in &tags {
                if task.tags.contains(&tag.to_string()) {
                    let index = &task.tags.iter().position(|t| t == tag);
                    match index {
                        Some(i) => task.tags.remove(*i),
                        _ => continue,
                    };
                }
            }
        }
        
        if verbose {
            println!("Deleted tag(s) {:?}", tags);
        }
    } else if let Some(matches) = matches.subcommand_matches("deleteTasks") {
        let names = matches.values_of("name").unwrap_or_default().collect::<Vec<&str>>();

        let mut removed_tasks = Vec::new();
        for name in &names {
            let mut index: Option<usize> = None;
            for task in &tasks.tasks {
                if &task.name.as_str() == name {
                    index = tasks.tasks.iter().position(|t| t == task);
                    removed_tasks.push(name);
                }
            }
            match index {
                Some(i) => &mut tasks.tasks.remove(i),
                _ => continue,
            };
        }

        if verbose {
            if removed_tasks.len() > 0 {
                println!("Deleted task(s) {:?}", removed_tasks);
            }
            else {
                println!("Task(s) does not exist: {:?}", names);
            }
        }
    } else if let Some(matches) = matches.subcommand_matches("practiceTask") {
        let name = String::from(matches.value_of("name").unwrap());
        let print_duration: i64;
        let mut practised = false;

        for task in &mut tasks.tasks {
            if task.name == name {
                let now: DateTime<Local> = Local::now();
                print_duration = matches.value_of("duration").unwrap_or(&task.duration.to_string()).parse::<i64>().unwrap();
                task.occurrences.push(TaskOccurrence {
                    timestamp: now.timestamp(),
                    duration: print_duration,
                });
                if verbose {
                    println!(
                        "Practiced task {} for {} minutes at {:02}:{:02} on {}/{}/{}",
                        name, print_duration, now.hour(), now.minute(), now.day(), now.month(), now.year()
                    );
                }
                practised = true;
                break;
            }
        }

        if !practised && verbose {
            println!("Could not find task: {}", name);
        }
    } else if let Some(matches) = matches.subcommand_matches("setTaskPracticeCount") {
        let name = String::from(matches.value_of("name").unwrap());
        let count = matches.value_of("count").unwrap().parse::<i64>().unwrap();
        let mut found = false;

        for task in &mut tasks.tasks {
            if task.name == name {
                found = true;
                loop {
                    if task.occurrences.len() as i64 == count {
                        if verbose {
                            println!("Practice count for task {} set to {}", name, count);
                        }
                        break;
                    } else if task.occurrences.len() as i64 <= count {
                        let now: DateTime<Local> = Local::now();
                        task.occurrences.push(TaskOccurrence {
                            timestamp: now.timestamp(),
                            duration: task.duration,
                        });
                    } else {
                        task.occurrences.pop();
                    }
                }
            }
        }

        if !found && verbose {
            println!("Could not find task: {}", name);
        }
    } else if let Some(matches) = matches.subcommand_matches("resetTaskPracticeCount") {
        let name = String::from(matches.value_of("name").unwrap());
        let mut found = false;

        for task in &mut tasks.tasks {
            if task.name == name {
                found = true;
                task.occurrences.clear();
                if verbose {
                    println!("Practice count for task {} reset", name);
                }
                break;
            }
        }

        if !found && verbose {
            println!("Could not find task: {}", name);
        }
    } else if let Some(matches) = matches.subcommand_matches("chooseTask") {
        let name = matches.value_of("name");
        let status = matches.value_of("status");
        let max_duration = matches.value_of("max-duration");
        let min_duration = matches.value_of("min-duration");
        let class = matches.value_of("class");
        let max_occurrences = matches.value_of("max-occurrences");
        let min_occurrences = matches.value_of("min-occurrences");
        let tags = matches.values_of("tag").unwrap_or_default().collect::<Vec<&str>>();
        // Convert tags from &str to String
        let tags = tags.iter().map(|tag| String::from(*tag)).collect();

        choose_task(&tasks.tasks, name, status, max_duration, min_duration, class, max_occurrences, min_occurrences, tags, json);
    } else if let Some(matches) = matches.subcommand_matches("updateTask") {
        let name = matches.value_of("name").unwrap();
        let status = matches.value_of("status");
        let duration = matches.value_of("duration");
        let class = matches.value_of("class");
        let new_name = matches.value_of("new-name");

        tasks = match update_task(tasks, name, status, duration, class, new_name, verbose) {
            Ok(t) => t,
            Err(error) => panic!("{}", report_error(error)),
        };
    }

    // Write tasks to tasks file in yaml
    dump_yaml_serde(tasks_file, &tasks);
}
