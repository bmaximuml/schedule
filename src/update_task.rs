use schedule::TaskYaml;

pub fn update_task(
    mut tasks: TaskYaml, name: &str, status: Option<&str>, duration: Option<&str>, class: Option<&str>, new_name: Option<&str>, verbose: bool
) -> Result<TaskYaml, String> {
    let mut found = false;
    for task in &mut tasks.tasks {
        if task.name == name {
            if verbose {
                println!("For task {}:", name);
            }
            found = true;
            match status {
                Some(s) => {
                    let new_status = s.to_string();
                    if verbose {
                        println!("  Updated status from <{}> to <{}>", task.status, new_status);
                    }
                    task.status = new_status;
                },
                _ => (),
            };
            match duration {
                Some(d) => {
                    let new_duration = d.parse::<i64>().unwrap();
                    if verbose {
                        println!("  Updated duration from <{}> to <{}>", task.duration, new_duration);
                    }
                    task.duration = new_duration;
                },
                _ => (),
            };
            match class {
                Some(c) => {
                    let new_class = c.to_string();
                    if verbose {
                        println!("  Updated class from <{}> to <{}>", task.class, new_class);
                    }
                    task.class = new_class;
                },
                _ => (),
            };
            match new_name {
                Some(n) => {
                    let new_name = n.to_string();
                    if verbose {
                        println!("  Updated name from <{}> to <{}>", task.name, new_name);
                    }
                    task.name = new_name;
                },
                _ => (),
            };
        }
    }

    if found {
        Ok(tasks)
    }
    else {
        Err(format!("Could not find task: {}", name))
    }
}