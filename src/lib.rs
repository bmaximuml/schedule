extern crate colored;

use colored::Colorize;
use serde::{Serialize, Deserialize};
use serde_json::json;
use std::fs;

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct TaskOccurrence {
    pub timestamp: i64,
    pub duration: i64,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Task {
    pub name: String,
    pub duration: i64,
    pub class: String, // required or optional
    pub status: String,
    pub occurrences: Vec<TaskOccurrence>,
    pub tags: Vec<String>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct TaskYaml {
    pub tasks: Vec<Task>,
}

pub fn load_yaml_serde(file: &str) -> TaskYaml {
    let yaml_str = match fs::read_to_string(file) {
        Ok(data) => data,
        Err(error) => panic!("{}", report_error(format!("Unable to read file: {} : {:?}", &file, error))),
    };

    let deserialized_map: TaskYaml = match serde_yaml::from_str(&yaml_str) {
        Ok(data) => data,
        Err(error) => panic!("{}", report_error(format!("Unable to serde_yaml: {} : {:?}", &yaml_str, error))),
    };
    // println!("{:#?}", deserialized_map);
    deserialized_map
}

pub fn dump_yaml_serde(file: &str, tasks: &TaskYaml) {
    // Wipe file before writing. serde write will overwrite existing content with existing, but won't pad existing to
    // wipe out excess data in file beyond what is written
    fs::File::create(file).expect("create failed");

    let f = fs::OpenOptions::new()
        .write(true)
        .create(true)
        .open(file)
        .expect("Couldn't open file");
    serde_yaml::to_writer(f, tasks).unwrap();
}

pub fn print_header() {
    println!(
        "| {0: <12} | {1: <11} | {2: <8} | {3: <8} | {4: <11} | {5: <20 } |",
        "name", "status", "duration", "class", "occurrences", "tags"
    );
    println!(
        "| {0: <12} | {1: <11} | {2: <8} | {3: <8} | {4: <11} | {5: <20 } |",
        "====", "======", "========", "=====", "===========", "===="
    );
}

pub fn print_task(task: &Task) {
    println!(
        "| {0: <12} | {1: <11} | {2: <8} | {3: <8} | {4: <11} | {5: <20} |", task.name, task.status, task.duration, 
        task.class, task.occurrences.len(), task.tags.join(",")
    );
}

pub fn print_task_json(task: &Task) {
    let json = json!(task);
    println!("{}", json.to_string());
}

pub fn print_tasks_json(tasks: &Vec<Task>) {
    let json = json!(tasks);
    println!("{}", json.to_string());
}

pub fn report_error(message: String) -> String {
    format!("{}", message.red())
}
